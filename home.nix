{ pkgs, ... }:
let 
  unstable = import <nixos-unstable> { config = { allowUnfree = true; }; };
in 
{
  imports = [
   ./gnome/gnome_extensions.nix
   ./zsh/zsh.nix
   ./hypr/hypr.nix
   ./hypr/swaylock.nix
   ./fonts/fonts.nix
   ./foot/foot.nix
   ./gtk/gtk.nix
  ];  
    home.username = "jart";
    home.homeDirectory = "/home/jart";
    nixpkgs.config.allowUnfree = true;
    home.stateVersion = "23.05";
    home.packages =  with pkgs; [ 
	lm_sensors
	vlc
	android-tools
	unstable.vscode.fhs
	google-chrome 
	python3
	neofetch 
	obs-studio
	tigervnc
	drawing
	mpvpaper
	lutris 
	telegram-desktop 
	gitkraken 
	git 
	gimp
	wineWowPackages.stable 
	winetricks 
	home-manager 
	neovim
	gnome.gnome-terminal 
	gnome.gedit
	gnome.gnome-tweaks 
	gnomeExtensions.user-themes
	gnomeExtensions.dash-to-dock
	unzip
	androidStudioPackages.dev
        meslo-lgs-nf
	zsh-powerlevel10k
	wget
	foot
	xfce.thunar
	clipman
	grim
	slurp
	swaybg
	wl-clipboard
	killall
	pavucontrol
	bc
        lazygit
        gnome.adwaita-icon-theme
        gnome.gnome-themes-extra
        libadwaita
        xfce.xfconf
        xfce.thunar-archive-plugin
        xfce.thunar-volman
        gvfs
	ddcutil
	filezilla
	anydesk
	dbeaver
	dunst
	swaylock
	swayidle
];  
}
