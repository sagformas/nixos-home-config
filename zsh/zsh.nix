{ pkgs, ... }:
let
  p10kTheme = if builtins.pathExists ./p10k.zsh then ./p10k.zsh else null;
  loadp10kTheme = if p10kTheme != null then "source ${p10kTheme}" else ""; 
in
{
  programs.zsh = {
    enable = true;
    shellAliases = {
	hmu = "home-manager switch";
	nxu = "doas nixos-rebuild switch"; 
	nxua = "doas nixos-rebuild switch --upgrade-all";
	nxc = "doas sh -c 'nix-collect-garbage --delete-old && nix-collect-garbage -d && nixos-rebuild boot && nixos-rebuild switch'";
        chm = "cd ~/.config/home-manager/";

    };
    initExtra = ''
       ${loadp10kTheme}
    '';

    plugins = [
      {
        name = "powerlevel10k";
        src = pkgs.zsh-powerlevel10k;
        file = "share/zsh-powerlevel10k/powerlevel10k.zsh-theme";
      }
      {
      name = "zsh-autosuggestions";
      src = pkgs.fetchFromGitHub {
        owner = "zsh-users";
        repo = "zsh-autosuggestions";
        rev = "v0.7.0";
        sha256 = "1g3pij5qn2j7v7jjac2a63lxd97mcsgw6xq6k5p7835q9fjiid98";
      };
    }
    ];
  };
}
